INSERT INTO users (email, password, admin)
values ('someone@someone.com', 'abcdefgh', false),
('someadmin@someadmin.com','admin',true);

INSERT INTO event (name, description, date, time, street, city, state, zipcode, latitude, longitude, category_id)
values ('Volunteer Event','This is the description of the Event', current_date, current_timestamp,'500 Stanton Road','Newark','DE','19713',100.0,200.0,1);