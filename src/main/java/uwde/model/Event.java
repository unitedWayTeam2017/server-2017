package uwde.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.logging.Logger;


@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String description;
    private String date;
    private String time;
    private String street;
    private String city;
    private String state;
    private String zipcode;
    private double latitude;
    private double longitude;
    private int categoryId;
    private ArrayList<String> volunteers = new ArrayList<>();

    private static final Logger logger = Logger.getLogger("Event.Model");



    public Event() {}



    public Event(String name, String date, String time, String description, String street, String city, String state, String zipcode, double latitude, double longitude, int categoryId) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.description = description;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.categoryId = categoryId;
    }

    public String toString() {
        return id + " - " + description + " - " + date + " - " + time + " - " + street + " - " + city + " - " + state + " - " + zipcode + " - " + latitude + " - " + longitude + " - " + categoryId;
    }

    public ArrayList<String> getVolunteers(){
        return this.volunteers;
    }

    public void addVolunteer(String userEmail){
        if( this.volunteers.contains(userEmail) ){
            logger.info("This User is already attending event.");
        } else {
            this.volunteers.add(userEmail);
        }

    }

    public void subtractVolunteer(String userEmail){
        this.volunteers.remove(userEmail);
    }


    public long getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getZipcode() {
        return zipcode;
    }
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public int getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
