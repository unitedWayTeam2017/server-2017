package uwde.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uwde.model.Event;

@Repository
public interface EventDAO extends JpaRepository<Event, Long> {
    Event findEventById(@Param("id") long id);
}
