package uwde.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uwde.model.User;

import java.util.List;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {

    User findUserByEmail(@Param("email") String email);
    }

