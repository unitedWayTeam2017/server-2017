package uwde.controller;

import uwde.dao.EventDAO;
import uwde.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uwde.model.User;

import java.util.logging.Logger;

@CrossOrigin
@RestController
public class EventController {

    private final String SUCCESS = "Success";
    private final String FAILURE = "Failure";
    private static final Logger logger = Logger.getLogger("EventController");

    @Autowired
    EventDAO eventDAO;

    @RequestMapping(value = "/event", method = RequestMethod.GET)
    public Iterable<Event> getEmployees() {
        return eventDAO.findAll();
    }

    @RequestMapping(value = "/create-event", method = RequestMethod.POST)
    public String createEvent(@RequestBody Event event) {
        try{

            logger.info(event.toString());

            eventDAO.save(event);
            logger.info(SUCCESS);
            return SUCCESS;
        } catch( Exception e ){
            logger.info(e.getMessage());
            return FAILURE;
        }
    }

    @RequestMapping(value = "/attend-event", method = RequestMethod.GET)
    public Event attendEvent(@RequestParam("eventIdAndUserEmail") String eventIdAndUserEmail) {
        try{

            String[] strings = eventIdAndUserEmail.split("-");
            long eventId = Integer.parseInt(strings[0]);
            String userEmail = strings[1];

            logger.info("*** Debug ***");
            logger.info("Event ID: " + eventId);
            logger.info("User Email: " + userEmail);

            Event event = this.eventDAO.findEventById(eventId);
            event.addVolunteer(userEmail);
            this.eventDAO.save(event);
            logger.info(event.getVolunteers().toString());

            return event;
        } catch( Exception e ){
            logger.info(e.getMessage());
            return new Event();
        }
    }

    @RequestMapping(value = "/un-attend-event", method = RequestMethod.GET)
    public Event unAttendEvent(@RequestParam("eventIdAndUserEmail") String eventIdAndUserEmail) {
        try{

            String[] strings = eventIdAndUserEmail.split("-");
            long eventId = Integer.parseInt(strings[0]);
            String userEmail = strings[1];

            logger.info("*** Debug ***");
            logger.info("Event ID: " + eventId);
            logger.info("User Email: " + userEmail);

            Event event = this.eventDAO.findEventById(eventId);
            event.subtractVolunteer(userEmail);
            this.eventDAO.save(event);
            logger.info(event.getVolunteers().toString());

            return event;
        } catch( Exception e ){
            logger.info(e.getMessage());
            return new Event();
        }
    }

    @RequestMapping(value = "/delete-event", method = RequestMethod.POST)
    public String deleteEvent(@ RequestBody Event event) {
        try{

            logger.info(event.toString());

            eventDAO.delete(event);
            logger.info(SUCCESS);
            return SUCCESS;
        } catch( Exception e ){
            logger.info(e.getMessage());
            return FAILURE;
        }
    }

}


