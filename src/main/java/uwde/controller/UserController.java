package uwde.controller;

import uwde.model.Login;
import uwde.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uwde.dao.UserDAO;

import java.util.ArrayList;
import java.util.logging.Logger;

import java.util.List;

@CrossOrigin
@RestController
public class UserController {

    private static final Logger logger = Logger.getLogger("EventController");

    @Autowired
    UserDAO userDAO;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Iterable<User> getEmployees(@RequestBody User user) {
        System.out.println(user);
        return userDAO.findAll();
    }

    @RequestMapping(value = "/getAdminUsers", method = RequestMethod.GET)
    public Iterable<User> getAdminUsers() {

        List<User> adminUsers = new ArrayList<>();
        List<User> users = userDAO.findAll();
        for(User user: users) {
            if(user.getAdmin()){
                adminUsers.add(user);
//                System.out.println(user);
            }
        }
//        System.out.println(adminUsers);
        return adminUsers;
    }

    @RequestMapping(value = "/setAdminUser", method = RequestMethod.GET)
    public String setAdminUser(@RequestParam("email") String email) {
        try {
            List<User> users = userDAO.findAll();

            for (User user : users) {
                if (user.getEmail().equalsIgnoreCase(email)) {
                    user.setAdmin(true);
                    userDAO.save(user);
                    return "User: " + email + ": is now Admin.";
                }
            }
            return "User: " + email + ": is not found.";

        } catch( Exception e ) {
            logger.info(e.getMessage());
            return "Error: When trying to set " + email + " to Admin.";
        }
    }

    @RequestMapping(value = "/removeAdminUser", method = RequestMethod.GET)
    public String removeAdminUser(@RequestParam("email") String email) {
        try {
            List<User> users = userDAO.findAll();

            for(User user: users) {
                if(user.getEmail().equalsIgnoreCase(email)){
                    user.setAdmin(false);
                    userDAO.save(user);
                    return "Admin Access Removed from: " + email;
                }
            }
            return "User: " + email + ": is not found.";

        } catch( Exception e ) {
            logger.info(e.getMessage());
            return "Error: When trying to set " + email + " to Admin.";
        }
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public User validLogin(@RequestBody Login login) {

        List<User> users = userDAO.findAll();

        for (User user : users) {
            if (login.getEmail().equals(user.getEmail())) {
                if (login.getPassword().equals(user.getPassword())) {
                    return user;
                }
            }
        }
        return null;
    }
}


